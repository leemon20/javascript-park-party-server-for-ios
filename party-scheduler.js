var mongoose = require('mongoose');
var validate = require("validate.js");
var moment = require('moment');
var Reminder = require('reminder');

var reminder = new Reminder();
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/park-party');

var ScheduledPartySchema = new Schema({
    startDate: {
        type: Date,
        default: Date.now
    },
    endDate: {
        type: Date,
        default: Date.now
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    party: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Party'
    }
});

var scheduledPartyConstraints = {
    startDate: {
        presence: true,
        datetime: true
    },
    endDate: {
        presence: true,
        datetime: true
    },
    creationDate: {
        presence: true,
        datetime: true
    },
    party: {
        presence: true
    }
};

var ScheduledParty = mongoose.model('ScheduledParty', ScheduledPartySchema);

var partyScheduler = {
    scheduleParty: function (cb, scheduleData) {
        var result = validate(scheduleData, scheduledPartyConstraints);

        if (result !== undefined) {
            return {
                error: result,
                msg: "",
                value: null,
                success: false
            };
        }

        new ScheduledParty(scheduleData).save(function (err, scheduledParty) {
            if (err) {
                cb({
                    error: "Couldn't save scheduled party to database",
                    msg: "",
                    value: null,
                    success: false
                });
            }

            remider.at('minute', function (date) {
                console.log("minute triggered");
                
                partyScheduler.finishParty(scheduledParty);
            });

            cb({
                error: "",
                msg: "Saved scheduled party",
                value: null,
                success: true
            });
        });
    },
    finishParty: function(scheduledParty) {
        console.log("going to mark party as finished", scheduledParty);
    }
};

module.exports = partyScheduler;