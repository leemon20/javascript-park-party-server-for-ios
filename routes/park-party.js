var express = require('express');
var mongoose = require('mongoose');
var validate = require("validate.js");
var router = express.Router();
var Schema = mongoose.Schema;

var PartySchema = new Schema({
    title: {
        type: String,
        default: '<no title>',
        trim: true
    },
    comment: {
        type: String,
        default: '',
        trim: true
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    participants: [String],
    active: {
        type: Boolean,
        default: true
    },
    longitude: Number,
    latitude: Number
});

var partyConstraints = {
    title: {
        presence: true,
        length: {
            minimum: 1,
            maximum: 20,
            message: "Title must be between 1 and 20 characters"
        }
    },
    longitude: {
        presence: true,
        numericality: {
            noStrings: true
        }

    },
    latitude: {
        presence: true,
        numericality: {
            noStrings: true
        }
    }
};

var Party = mongoose.model('Party', PartySchema);

router.param('partyID', function (req, res, next, partyID) {
    console.log('Retrieving party by id: ' + partyID);

    req.party = null;

    Party.findById(partyID, function (err, party) {
        if (!err) {
            console.log("found party: ", JSON.stringify(party));
            req.party = party;
            next();
        } else {
            console.log("party not found", partyID);
            next();
        }
    });
});

router.param('deviceID', function (req, res, next, deviceID) {
    console.log('Setting device id: ' + deviceID);

    req.deviceID = deviceID;

    next();
});

router.get("/id/:partyID", function (req, res, next) {
    var party = req.party;

    console.log("asked for party: ", party);

    if (party !== null) {
        if (party.active === true) {
            res.send({
                error: "",
                msg: "",
                value: party,
                success: true
            });
        } else {
            res.send({
                error: "Party already closed",
                msg: "",
                value: null,
                success: false
            });
        }
    } else {
        res.send({
            error: "Couldn't find Party.",
            msg: "",
            value: null,
            success: false
        });
    }
});

/* GET all parties */
router.get('/all', function (req, res, next) {
    Party.find(function (err, parties) {
        if (!err) {
            parties = parties.filter(function (party) {
                return party.active === true
            })

            console.log("all parties: " + JSON.stringify(parties));
            res.send({
                error: "",
                msg: "All Parties",
                value: parties,
                success: true
            });
        } else {
            res.send({
                error: "Couldn't access Parties",
                msg: "",
                value: [],
                success: false
            });
        }
    });
});

function createParty(cb, partyData) {
    console.log("creating party with data: ", partyData);

    new Party(partyData).save(function (err, party) {
        if (err) {
            cb({
                error: "Couldn't create Party",
                msg: "",
                value: null,
                success: false
            });
        }

        cb({
            error: "",
            msg: "Party created",
            value: party,
            success: true
        });
    });
}

/* POST create new party */
router.post('/new', function (req, res, next) {
    var partyData = req.body;
    var result = validate(partyData, partyConstraints);

    console.log("validatiting: " + JSON.stringify(partyData), "result:" + JSON.stringify(result));

    if (result === undefined) {
        Party.find({
            title: partyData.title
        }, function (err, parties) {
            if (!err) {
                if (parties.length === 0) {
                    createParty(function (result) {
                        res.send(result);
                    }, partyData);
                } else {
                    var busy = parties.some(function (party) {
                        return party.active === true;
                    });

                    if (busy) {
                        res.send({
                            error: "Party already exists",
                            msg: "",
                            value: null,
                            success: false
                        });
                    } else {
                        createParty(function (result) {
                            res.send(result);
                        }, partyData);
                    }
                }
            } else {
                res.send({
                    error: "Couldn't access Parties",
                    msg: "",
                    value: [],
                    success: false
                });
            }
        });
    } else {
        res.send({
            error: JSON.stringify(result),
            msg: "",
            value: null,
            success: false
        });
    }
});

/* GET create new party */
router.get('/close/:partyID', function (req, res, next) {
    var party = req.party;

    console.log("going to close party: ", party);

    if (party !== null) {
        if (party.active === true) {
            party.active = false;

            party.save(function (err, party) {
                if (err) {
                    res.send({
                        error: "Couldn't close Party",
                        msg: "",
                        value: null,
                        success: false
                    });
                }

                res.send({
                    error: "",
                    msg: "Party closed",
                    value: party,
                    success: true
                });
            });
        } else {
            res.send({
                error: "Couldn't close Party. Already closed",
                msg: "",
                value: party,
                success: false
            });
        }
    } else {
        res.send({
            error: "Couldn't close Party. Party not found",
            msg: "",
            value: null,
            success: false
        });
    }
});

/* GET join party, deviceID example = 79177027-9956-487E-8BF5-082E724EFFB5 */
router.get('/join/:partyID/device/:deviceID', function (req, res, next) {
    var party = req.party;

    console.log("going to join party: ", party, ", with device id:", req.deviceID);

    if (party !== null) {
        if ( (party.active === true) && (party.participants.indexOf(req.deviceID) === -1) ) {
            party.participants.push(req.deviceID);

            party.save(function (err, party) {
                if (err) {
                    res.send({
                        error: "Couldn't join Party",
                        msg: "",
                        value: null,
                        success: false
                    });
                }

                res.send({
                    error: "",
                    msg: "Joined party",
                    value: party,
                    success: true
                });
            });
        } else {
            res.send({
                error: "Already joined",
                msg: "",
                value: party,
                success: false
            });
        }
    } else {
        res.send({
            error: "Party not found",
            msg: "",
            value: null,
            success: false
        });
    }
});

/* GET leave party */
router.get('/leave/:partyID/device/:deviceID', function (req, res, next) {
    var party = req.party;

    console.log("going to leave party: ", party);

    if (party !== null) {
        if ( (party.active === true) && (party.participants.indexOf(req.deviceID) !== -1) ) {
            party.participants = party.participants.filter(function(id) {return id !== req.deviceID})

            party.save(function (err, party) {
                if (err) {
                    res.send({
                        error: "Couldn't leave Party",
                        msg: "",
                        value: null,
                        success: false
                    });
                }

                res.send({
                    error: "",
                    msg: "Party left",
                    value: party,
                    success: true
                });
            });
        } else {
            res.send({
                error: "Couldn't leave Party. Already closed",
                msg: "",
                value: party,
                success: false
            });
        }
    } else {
        res.send({
            error: "Couldn't leave Party. Party not found",
            msg: "",
            value: null,
            success: false
        });
    }
});

module.exports = router;
